#include <iostream>
#include <string>
#include <vector>
#include <mutex>
#include <condition_variable>

#include <boost/program_options.hpp>

#include <felix_proxy/ClientThread.h>
#include <felix/felix_client_properties.h>
#include <felix/felix_client_status.h>
#include <felix/felix_client_exception.hpp>


struct {
    std::string message;
    uint64_t subscribe_tag;
    uint64_t send_tag;

    uint64_t messages_received;
    uint64_t truncated;
    uint64_t crc;
    uint64_t error;

    int connections_up;
    uint64_t iterations;
} statistics;

FelixClientThreadInterface::Config config;

class UserClass {

public:
    /**
     * @brief Constructor of a user class.
     * Inside this function a client object is created, a
     */
    UserClass() {
        try {

            // Bind UserClass methods to API callbacks.
            // These callbacks are run by the event loop thread.
            config.on_init_callback = std::bind(&UserClass::on_init, this);
            config.on_data_callback = std::bind(&UserClass::on_data, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
            config.on_connect_callback = std::bind(&UserClass::on_connect, this, std::placeholders::_1);
            config.on_disconnect_callback = std::bind(&UserClass::on_disconnect, this, std::placeholders::_1);

            // Create a client
            client = new felix_proxy::ClientThread(config);

            // Subscribe to the e-link where replies will come from
            // and establish connection to send data
            client->subscribe(statistics.subscribe_tag);
            client->init_send_data(statistics.send_tag);

            
            // The first message will be sent in the on_connect callback.
            // Replies are be reported by the on_data callback that 
            // pushes them on a queue and send the next requests.
            //
            // As all callbacks are run by the event loop thread this
            // thread can either sleep or used to process data.
            //
            // The on_data callback notifies this thread that data is available
            // the queue. This threads pop the queue and processes the message.
            // The lock is released before starting message processing.
            while(true){
                std::unique_lock<std::mutex> lk(mtx);
                data_cond.wait(lk, [this]{return !fifo.empty(); });
                auto msg = fifo.front();
                fifo.pop();
                lk.unlock();
                process_data(msg);
                if(statistics.messages_received >= statistics.iterations){
                    std::cout << "All " << statistics.messages_received << " messages received."
                              << " Trucated " << statistics.truncated
                              << " Error " << statistics.error
                              << " CRC " << statistics.crc << std::endl;
                    break;
                }
            }

        } catch (std::invalid_argument const& error) {
            std::cerr << "Argument or option of wrong type" << std::endl;
            std::cout << std::endl;
            std::exit(-1);
        } catch (FelixClientResourceNotAvailableException& error) {
            std::cerr << "Failed to send, resource not available. Check felix bus." << std::endl;
            exit(1);
        }
    }

    ~UserClass() {
        delete client;
    }

private:

    felix_proxy::ClientThread* client;
    std::mutex mtx;
    std::condition_variable data_cond;
    std::queue<std::vector<uint8_t>> fifo;


    /**
     * @brief Callback invoked at start of the event loop.
     */
    void on_init() {
        std::cout << "on_init called" << std::endl;
    }


    /**
     * @brief Callback invoked for each established connection.
     * Here we require both "send" and "receive" connections to be up
     * before sending the first message.
     */
    void on_connect(uint64_t fid) {
        std::cout << "on_connect called for fid 0x" << std::hex << fid << std::endl;
        ++statistics.connections_up;
        if (statistics.connections_up == 2 ) {
            try {
                client->send_data(statistics.send_tag, (const uint8_t*)statistics.message.c_str(), statistics.message.size() + 1, true);
            } catch (FelixClientException& error){
                std::cout << error.what() << std::endl;
            }
        }
    }


    /**
     * @brief Callback invoked for each closed connection.
     * Just printing a message in this example.
     */
    void on_disconnect(uint64_t fid) {
        std::cout << "on_disconnect called for fid 0x" << std::hex << fid << std::endl;
    }


    /**
     * @brief Message reception callback.
     * This function shall not be blocking to avoid starvation of network resources.
     * In this example received messages are copied in an std::queue and
     * a new message is sent.
     * Note that send_data() can be called by the application thread.
     */
    void on_data(uint64_t fid, const uint8_t* data, size_t size, uint8_t status) {
        // This function is called for each message that is received
        // and is executed by the event loop thread and shall be non-blocking.

        ++statistics.messages_received;
        std::cout << "on_data call #" << statistics.messages_received 
                  << " fid 0x" << std::hex << fid << std::dec
                  << " size " << size << " status " << (unsigned int)status << std::endl;

        if (status == FELIX_STATUS_FW_TRUNC || status == FELIX_STATUS_SW_TRUNC) {
            ++statistics.truncated;
        }
        if (status == FELIX_STATUS_FW_CRC) {
            ++statistics.crc;
        }
        if (status == FELIX_STATUS_FW_MALF || status == FELIX_STATUS_SW_MALF) {
            ++statistics.error;
        }

        //copy message to a fifo  
        std::vector<uint8_t> msg(data, data+size);
        {
            std::lock_guard<std::mutex> lk(mtx);
            fifo.push(std::move(msg));
        }
        data_cond.notify_one();
        client->send_data(statistics.send_tag, (const uint8_t*)statistics.message.c_str(), statistics.message.size() + 1, true);
    }


    /**
     * @brief Dummy data process function.
     * This function checks that the received message is the same
     * as the sent one.
     */
    void process_data(std::vector<uint8_t> &msg) {
        char* msg_ptr = reinterpret_cast<char*>(msg.data());
        int ret = strcmp(msg_ptr, statistics.message.c_str());
        if ( ret != 0 ){
            std::cout << "ERROR: received "<< std::string(msg_ptr) << " expected " << statistics.message << std::endl;
        }
    }

};


using namespace boost::program_options;

int main(int ac, char *av[]) {

    options_description description("Options");

    description.add_options()
        ("help,h", "produce help message")
        ("log-level,l", value<std::string>()->default_value("info"), "logging (trace, debug, info, notice, warning, error, fatal)")
        ("verbose-bus,v", "Show the bus information")
        ("ip,i", value<std::string>()->default_value("127.0.0.1"), "IP of the local interface")
        ("send-fid,s", value<std::string>()->default_value("0x1000000000008000"), "send message to this fid")
        ("reply-fid,r", value<std::string>()->default_value("0x1000000000000000"), "receive reply from this fid")
        ("message,m", value<std::string>()->default_value("hello"), "message")
        ("bus-dir,b", value<std::string>()->default_value("./bus"), "bus directory")
        ("bus-group-name,g", value<std::string>()->default_value("FELIX"), "bus group name")
        ("timeout,t", value<std::string>()->default_value("0"), "subscription timeout")
        ("iterations,n", value<std::string>()->default_value("100"), "number of iterations");

    variables_map args;
    try {
        store(parse_command_line(ac, av, description), args);
        notify(args);
    } catch (error &ex) {
        std::cerr << ex.what() << std::endl;
        description.print(std::cout);
        return 1;
    }

    if (args.count("help")) {
        std::cout << "FELIX client application using felix-client-thread API." << std::endl;
        std::cout << "This application sends a message to a FELIX e-link," << std::endl;
        std::cout << "waits for a reply and sends another message." << std::endl;
        std::cout << "The number of iterations is set by the user." << std::endl;
        description.print(std::cout);
        return 0;
    }

    config.property[FELIX_CLIENT_LOCAL_IP_OR_INTERFACE] = args["ip"].as<std::string>();
    config.property[FELIX_CLIENT_LOG_LEVEL] = args["log-level"].as<std::string>();
    config.property[FELIX_CLIENT_BUS_DIR] = args["bus-dir"].as<std::string>();
    config.property[FELIX_CLIENT_BUS_GROUP_NAME] = args["bus-group-name"].as<std::string>();
    config.property[FELIX_CLIENT_VERBOSE_BUS] = args.count("verbose-bus") ? "True" : "False";
    config.property[FELIX_CLIENT_TIMEOUT] = args["timeout"].as<std::string>();
    // PAGE settings only for expert use.
    //config.property[FELIX_CLIENT_NETIO_PAGESIZE] = "512";
    //config.property[FELIX_CLIENT_NETIO_PAGES] = "128";

    statistics.send_tag = std::stol(args["send-fid"].as<std::string>(), nullptr, 0);
    statistics.subscribe_tag = std::stol(args["reply-fid"].as<std::string>(), nullptr, 0);
    statistics.message = args["message"].as<std::string>();
    statistics.iterations = std::stol(args["iterations"].as<std::string>(), nullptr, 0);
    statistics.truncated = 0;
    statistics.crc = 0;
    statistics.error = 0;
    statistics.connections_up = 0;

    UserClass uc = UserClass();
}

